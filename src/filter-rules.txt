! Filter rules not covered by the uBlock Protector filter list.
! uBO extended syntax for script injection CAN BE USED in this file, as it will be converted to Javascript in the non-uBO userscript.
! Any other uBO extended syntax filters will be ignored.

! altervista.org
! Issues: #36
/wp-content/uploads/$script,domain=altervista.org

! darmowe-pornosy.pl
! Issues: #83
darmowe-pornosy.pl###porno_accept

! eurogamer.net
! Issues: #18
eurogamer.net##script:inject(abort-on-property.read.js, stop)

! handelsblatt.com
! Issues: #71
handelsblatt.com##script:inject(abort-on-property-read.js, AdController)

! nana10.co.li
! Issues: #21
@@||cloudvideoplatform.com/advert.jpg$image,domain=nana10.co.il

! transparentcalifornia.com
! Issues: #15
transparentcalifornia.com##script:inject(overlay-buster.js)

! uol.com.br
! Issues: #89
@@||jsuol.com.br/c/detectadblock/$script,domain=uol.com.br,badfilter

! vidoza.net
! Issues: #68
@@||vidoza.net/js/ads.js$script
@@||vidoza.net/js/pop.js$script

! vtm.be
! Issues: #27
||35.184.169.188^$popup
@@||fwmrm.net^$xmlhttprequest,domain=vtm.be
